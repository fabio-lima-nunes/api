const express = require('express');
const app = express();
let Articles = require('./entities/articles.js');
let Comments = require('./entities/comments.js');

app.use(express.json());

// Listar todos os comentários
app.get('/api/comments', (req, res) => {
    return res.json(Comments);
})

// Listar comentários de um artigo
app.get('/api/comment/list/:id', (req, res) => {
    const {id} = req.params;
    const article = Comments.filter(comment => comment.content_id == id);

    if (!article) return res.status(204).json();

    return res.json(article);
});

// Listar comentário específico
app.get('/api/comment/:id', (req, res) => {
    const {id} = req.params;
    const client = Comments.find(comment => comment.id == id);

    if (!client) return res.status(204).json();

    return res.json(client);
});

// Cria um comentário novo
app.post('/api/comment/new', (req , res) => {
    const id =  Math.floor(Math.random() * 99999);
    const { comment, email, content_id } = req.body
    const bodyData = { id, email, comment, content_id, created_at: new Date() }
    Comments.push(bodyData)
    return res.json(bodyData)
});

// Apaga um comentário
app.delete('/api/comment/:id', (req, res) => {
    const {id} = req.params;
    Comments.filter(comment => comment.id != id);

    return res.json(Comments);
})

// Listar todos os artigos
app.get('/api/articles', (req, res) => {
  return res.json(Articles);
})

// Listar artigo específico
app.get('/api/article/:id', (req, res) => {
  const {id} = req.params;
  const article = Articles.find(article => article.id == id);

  if (!article) return res.status(204).json();

  return res.json(article);
});

// Cria um Artigo
app.post('/api/articles/new', (req, res) => {
    const id =  Math.floor(Math.random() * 99999);
    const { content } = req.body
    const bodyData = { id, content, create_at: new Date() }
    
    Articles.push(bodyData)
    return res.json(bodyData)
});

// Apaga um artigo
app.delete('/api/article/:id', (req, res) => {
    const { id } = req.params;
    Articles = Articles.filter(article => article.id != id);
    Comments = Comments.filter(comment => comment.content_id != id);
    
    return res.json(Articles);
})

// Inicia um servidor
app.listen(3000, () => {
    console.log("Server is running");
})
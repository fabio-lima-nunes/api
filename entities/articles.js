module.exports = Article = [
  {
    id: 1,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat, justo sit amet elementum posuere, nisi odio placerat lectus, iaculis tempor ligula sem at ante. In eget est maximus, porttitor tellus id, tincidunt lacus. Morbi tincidunt tortor ac feugiat laoreet. Fusce sem turpis, semper non condimentum a, fermentum egestas neque. Fusce risus elit, porttitor eget auctor at, eleifend a arcu. Etiam pharetra eros pharetra aliquam facilisis. Vivamus molestie ex in dolor fermentum molestie. Maecenas sagittis risus sapien, vitae tempus nisl molestie at. Morbi tristique massa porttitor nisi feugiat ornare. Praesent non purus non tortor malesuada tempus. Quisque tellus justo, ultricies ac tincidunt sit amet, tincidunt sit amet mi. Cras ut ligula iaculis, euismod augue vitae, mattis velit. Suspendisse sit amet dui luctus, bibendum ex egestas, maximus mi.',
    create_at: new Date()
  },
  {
    id: 2,
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elit augue, tempor nec urna a, sodales feugiat orci. Donec nibh risus, faucibus mattis dolor ut, luctus blandit augue. Sed pharetra pulvinar metus hendrerit elementum. Nam dignissim, odio eget aliquam pellentesque, metus mauris tincidunt neque, ac faucibus elit diam nec dolor. Nunc vehicula lacus dolor, eget tempor nisi lobortis at. Curabitur sollicitudin in sapien eu viverra.',
    create_at: new Date()
  },
  {
    id: 3,
    content: 'Integer efficitur scelerisque sagittis. In id luctus odio. Vivamus quis congue lorem. Vestibulum vestibulum mollis lobortis. Proin aliquam cursus massa, eu sollicitudin nibh pulvinar ac. Donec lacinia, neque at fermentum hendrerit, velit ex volutpat urna, non posuere est ex ac risus. Fusce hendrerit erat eu sollicitudin egestas. Nam vitae gravida tellus, eu cursus libero. Nullam bibendum dolor ac nulla commodo, at eleifend leo finibus. Suspendisse pellentesque sem quis rhoncus laoreet. Pellentesque urna nunc, tempor sit amet porttitor at, varius sit amet nulla. Etiam mollis et turpis in fringilla. Praesent imperdiet nisi sit amet consequat facilisis. Ut non libero augue. Donec luctus aliquet lorem non consectetur.',
    create_at: new Date()
  }
]
# README #

Este projeto foi feito baseado no link abaixo
https://github.com/gustavocastrow/estudo-api

Vale a pena ressaltar também que obtive a ajuda de alguns amigos programadores para alterarmos o código e fazer funcionar.

# Rotas da aplicação:
Listar todos os comentários
curl -sv 3.81.109.65:3000/api/comments

Listar comentários de um artigo
curl -sv 3.81.109.65:3000/api/comment/list/

Listar comentário específico
curl -sv 3.81.109.65:3000/api/comment/id

Cria um comentário novo
curl -sv 3.81.109.65:3000/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"alice@example.com","comment":"first post!","content_id":1}'

Cria uma matéria
curl -sv 3.81.109.65:3000/api/articles/new -X POST -H 'Content-Type: application/json' -d '{"email":"bob@example.com","comment":"I guess this is a good thing","content_id":2}'

Lista matéria
curl -sv 3.81.109.65:3000/api/article/list/1

```
